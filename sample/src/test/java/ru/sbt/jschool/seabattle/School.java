package ru.sbt.jschool.seabattle;

import java.io.Serializable;

/**
 * Created by roma on 02.04.2019.
 */
public class School implements Serializable {
    private String name;
    private String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof School)) return false;

        School school = (School) o;

        if (getName() != null ? !getName().equals(school.getName()) : school.getName() != null) return false;
        return getAddress() != null ? getAddress().equals(school.getAddress()) : school.getAddress() == null;

    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + (getAddress() != null ? getAddress().hashCode() : 0);
        return result;
    }
}
